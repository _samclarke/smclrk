---
layout: page
title: About
---

{% image https://dl.dropboxusercontent.com/u/4298648/portrait.jpg %}

My name is Sam Clarke. I currently live in Melbourne, Australia with my beautiful fiancé, [Charleigh](http://www.charleighbest.co.uk), our daughter, Elliotte, and a soon to be born baby!

I generally write about my life; family, experiences, and travel, my passion for photography tends to bleed through a little but you can find my favourites [here](http://www.samclarke.photography).

To see more of what I've written, take a look through the [archives](/blog)</a>.
