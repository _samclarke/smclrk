---
layout: post
title: Proud isn't the word
date: 2013-11-19 22:05:19.000000000 +11:00
---
Yesterday marked a landmark for Charleigh, for her list of goals and for our planned future... It was her first day of work! This isn't to say she hasn't worked in the past, only that Ellie is now at an age where she can attend nursery, so Charleigh is able to bring home some bacon!<br/><br/>From what I understand, yesterday was more of an interview day than an actual first day of work... She pretty much spent time going through the usual rigmarole of finding out more about the job, meeting people, get her rota etc. But even after a day of all those kinds of things, Charleigh came home absolutely ecstatic with the way it went. I was pleased, I'd have hated for her first interaction in the working world to be horrible, but more importantly than all of this, she's keen, she can't wait to get working, get more involved with the business and meet new people. This is all working towards us moving to our house in April and this was definitely one of the biggest steps.<br/><br/>As I said before, I couldn't be more proud of her, she really deserves all the "happyness" she gets!
