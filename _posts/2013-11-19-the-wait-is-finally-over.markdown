---
layout: post
title: The wait is finally over!
date: 2013-11-19 22:05:19.000000000 +11:00
---
If you were awake at the ungodly hour of 10:30am (Australian time) today, you may have noticed me frantically tweeting that the wait is finally over.. Charleigh and Ellie's VISAs have been approved!<!--more-->
<br/><br/>This is the moment we've been waiting for, for 7 weeks today. Nothing else I've experienced in life has been quite so difficult or stressful, though the rewards will be truly incredible. We're now hunting for the best/cheapest flights we can find, though inevitably I would imagine this will rely mostly on Charleigh finalising things at home, work etc. and my booking of flights.<br/><br/>We're almost there and I couldn't be more pleased.
