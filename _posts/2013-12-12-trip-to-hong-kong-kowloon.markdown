---
layout: post
title: 'Trip to Hong Kong: Kowloon'
date: 2013-12-12 21:58:28.000000000 +11:00
---

{% image http://farm3.staticflickr.com/2843/11336978716_f70fdff7b9_b.jpg "Kowloon" %}

This was my penultimate day in Hong Kong and I made a trip over to the Kowloon side of the City. I walked around the streets, along the bay and through a handful of shops, snapping a few pictures on the way. 

[Click through for more.](http://www.flickr.com/photos/sam_clarke/11336978716/)
