---
layout: post
title: People are interested in the most inane of things
date: 2013-11-19 22:05:19.000000000 +11:00
---
<p style="text-align: center;"><img class="size-full wp-image-346 aligncenter" title="Top Tweet" src="http://www.samuelclarke.co.uk/wp-content/uploads/2012/05/dasright.png" alt="" width="586" height="205" /></p><br/>This morning I found myself in an odd predicament, whereby a tweet I posted on the hashtag #MostCommonLies, made it's way to the top of that trending section for the morning. This brought with it an absolute onslaught of tweets; the usual garbage, following requests, "wand erection" fans... all sorts. This brings me to wonder, why are people interested in tweets like this? More poignantly, why does a tweet of this nature, bring with it such an influx of spam accounts, masses of retweets and hundreds of follows?<br/><br/>Why aren't people interested in decent articles that find themselves floating through our feeds? Is the IQ of the average internet user really that low?
