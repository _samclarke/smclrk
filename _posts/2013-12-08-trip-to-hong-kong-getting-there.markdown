---
layout: post
title: 'Trip to Hong Kong: Getting there'
date: 2013-12-08 13:46:36.000000000 +11:00
---
<center><iframe src="http://www.flickr.com/photos/sam_clarke/11268183266/player/" width="750" height="500" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe></center>

I would consider myself fairly well travelled at this point in my life. I've been fortunate to travel round the majority of Europe, both sides of the US, India and I've lived in Australia for the better part of two years now - But that's not to say there's any chance of me slowing down!

A large majority of my more recent trips have been through my work and this week I'm extremely fortunate to be based out of our Hong Kong office.

Given the amount of trips I've undertaken, travelling itself is neither here nor there for me anymore. I set off from Melbourne airport at 00:30 on Sunday morning, flying with Cathay Pacific (a first for me). It's only a 9 hour flight to Hong Kong and so by the time I'd watched [Top Gear: The Perfect Road Trip](http://www.imdb.com/title/tt3258142/), had my dinner and a quick sleep, we were already on the descent in to Hong Kong international airport.

I have to give credit to Cathay Pacific, their in-flight staff were fantastic, the food was (perhaps surprisingly for airline food) pretty good and the time it took to get through security and immigration once I got to Hong Kong was less than the time it took for my bag to get to the carousel!

After getting out of the airport, I caught the Airport Express in to Central and spent a good couple of hours wandering the streets, familiarising myself with the area. 

I'm now sat it my hotel lounge, charging my plethera of devices and waiting for my room to be cleaned before I take a shower and head out in to Hong Kong! I'm really looking forward to seeing more of this place, but I'm already surprised (not quite sure in which way, yet) by just how influenced this area was (and still is) by British Rule. I'll be getting as many pictures as possible so perhaps I'll look in to this in a bit more detail next time.
