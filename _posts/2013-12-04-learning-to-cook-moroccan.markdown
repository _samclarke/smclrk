---
layout: post
title: Learning to cook Moroccan
date: 2013-12-04 21:19:33.000000000 +11:00
---
<center><iframe src="http://www.flickr.com/photos/sam_clarke/11214438766/player/" width="750" height="500" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe></center>

Cooking has always been a huge passion of mine, from an early age I always spent time learning to cook with my Mum. Learning the basics at a young age gave me a real boost in confidence when it came to cooking, but it wasn't until Charleigh and Ellie came in to my life that I really started to become more diverse in what I was cooking.

Growing up with a nut allergy, I found myself constantly excusing myself from certain dishes or restaurants entirely - but as I've grown, I've learned to appreciate many more flavours and styles of food. This made our offices Christmas party an incredible treat for me, not only did we get to spend the day with 2 professional chefs, we also got to learn about and cook a cuisine which was entirely new to me.

Click through on the picture above to see more of our amazing day.
