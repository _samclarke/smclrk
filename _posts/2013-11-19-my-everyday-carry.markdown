---
layout: post
title: My Everyday Carry
date: 2013-11-19 22:05:19.000000000 +11:00
---
<p style="text-align: center;"><a href="http://www.flickr.com/photos/sam_clarke/8982183513/"><img class="wp-image-249 aligncenter" alt="My Everyday Carry" src="http://smclrk.com/wp-content/uploads/2013/06/Everyday-Carry-1024x682.jpg" width="1024" height="682" /></a></p>
I've been a really big fan of EDCs for a while now, it saddens me that <a href="http://everyday-carry.com/">Everyday Carry</a> isn't up and running anymore, so I wanted to post my typical day carry. If I'm carrying my back I tend to also have my laptop on me as well, but this covers the majority!

iPad Mini
iPhone 5
Ray-ban Glasses
Status Anxiety Wallet
Sennheiser CX300II Headphones
Komono Watch
Parker Ballpoint Pen
Carmex
Lighter
