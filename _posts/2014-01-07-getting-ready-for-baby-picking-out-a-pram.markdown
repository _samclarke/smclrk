---
layout: post
title: 'Getting ready for baby: Picking out  a pram'
date: 2014-01-07 14:20:13.000000000 +11:00
---
As we draw closer to the day we expect to meet our baby, the list of items I anticipate buying seems to be growing longer by the day. Charleigh has remained fairly aprehensive about the amount of items we'll genuinely need, maintaining a shortlist based on her past experience.

{% image alignleft /uploads/2014/03/joolz-day-pram.jpg "Joolz Day Pram" %}

Given the current space in our apartment, or lack thereof, I totally understand why we should be looking to hold back on too many of the bulkier items - That is until we find a larger home to help house our growing family. One thing is for certain however, we're definitely going to need a pram (Read: Buggy, Stroller, Push-chair).

I initially began looking in to prams almost immediately after we found out we were to be expecting our baby. From that moment there were two aspects to our choice that I knew I had to get absolutely spot on:

{% image alignright /uploads/2014/03/uppa-baby-vista.jpg "Uppababy Vista" %}

####Multi-purpose Design

Since we're looking to save on space in the short term and ultimately to save of money, we needed to make sure that whichever pram we chose would allow to to utilise a car-seat as the bassinet and also allow for adaption to different seats as our baby grows.

####Correct ergonomic height

Being as I'm 6' 2", I didn't want want to be hunched over in agony pushing a chair designed for Charleigh's shorter frame. This is vitally important, since the 10+ hours I spend sat in front of my computer is already far too much for my back to take.

Whittling down the options, taking in to account our "must haves" - Completely dismissing any product with a bad review, or so much as a safety related product recall during the companies early years, I was eventually left with two choices.

Whilst visually very similar there are a couple of distinct differences between these two prams, ultimately the UppaBaby Vista was **3" taller**, **1.5kg lighter** and **$555 cheaper**. It's a complete no-brainer - It's increased height is a huge advantage for me, the small but important weight difference will be very helpful for Charleigh, and the money saved is more than enough for us to buy a car-seat (Once we have a car, anyway).


I'm glad I spent so much time working my way through viable options and now I can't wait to have the pram in my hands. It makes the realisation that our baby is almost here, so much clearer!