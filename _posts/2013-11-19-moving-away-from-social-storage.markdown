---
layout: post
title: Moving Away From Social Storage
date: 2013-11-19 22:05:19.000000000 +11:00
---
I've long been a user of many different online services over the years I've been online, from some of the early social networking tools right the way up to the now more common and much more involved and automatic life-logging services. I'd always been a great fan of these kinds of applications, especially those which allowed me to upload, store and share my pictures.

Over the past few years the ways in which we all use these types of sites/applications has exploded. Most of my friends and family went from using these services as infrequent, useful ways of communication to 99% of their lives being distributed online within a matter of months. This had been fantastic (especially in the last couple of years whilst I've been living in Australia) as ways of sharing where we are, what we're doing and the pictures of those events - All in a domain that was completely separate from time.

More recently, I've begun to take photography much more seriously. It's no longer just something I'm using to capture moments, or share things - I'm now starting to build out quite the catalog of pictures that I want to keep in a much more personal sense than sharing online.

Whilst I've found the likes of Instagram &amp; Facebook really useful, there are two very distinct reasons for which I'm now beginning to move away from these platforms:

<strong>I want to be more selective with what I place online.</strong>

What I mean by that is I've always tended to overuse social networking. Over-sharing of information isn't my major concern here, whilst an issue in it's own right, I see that more as something that I've always managed to be fairly safe with; I don't post levels of personal information that could possibly endanger myself and certainly don't make myself a target for online fraud.

My use of the term "overuse of social network" is really aimed towards my photos. I'm not the type of Facebook member with 12,000 tagged photos, nor do I post images of ever meal, but I certainly take digital photography for granted and that's leading to something I really want to resolve: The quality of my photos are low because the volume of those picture are high.

<strong>I want to own what's mine.</strong>

This is a subject that's been written by just about everyone in the last couple of months, so I won't go in to the unnecessary details, but ultimately I don't want to be giving away my personal pictures (being shared for friends &amp; family to see) to anyone who feels they'd make a fantastic backdrop on the next advertising campaign of a mid-market upholsterer.

To this extent two things are now happening. I'll no longer be posting quite the amount of pictures that I used to, to the majority of social networks. Instead I'll be using my own site, my own domain, as a place to store the images I feel I want to share, and keeping almost every other image privately.
