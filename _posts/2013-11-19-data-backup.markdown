---
layout: post
title: Data backup
date: 2013-11-19 22:05:19.000000000 +11:00
---
Unfortunately, this is not the first time this has happened... But I recently sold my Macbook Pro, before doing so I spent hours, working through all my music, videos and personal files to ensure that when I formatted the drive, I didn't lose anything.<br/><br/>It wasn't until some weeks after I sold my MBP that I realised I had not backed up iPhoto. Because of this, I unfortunately lost almost every picture I had taken over the last 4 years, including pictures from my trip to India, Skiing holidays and my travels through the east coast of the states. Needless to say, I was heartbroken.<br/><br/>I've sworn now, that not a single picture or video I ever take, will be lost to this stupidity. I've ordered a 2TB NAS drive, to which I will be linking all my computers and mobile devices, to ensure that nothing is ever lost again. I just couldn't handle this happening once more.
