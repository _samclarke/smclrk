---
layout: post
title: 'Scan Number 1: Our Baby''s Life So Far'
date: 2013-11-19 22:05:19.000000000 +11:00
---
This week saw the first in a line of exciting events for us as a family, as we went to an appointment at Future Medial Imaging Group, for our initial ultrasound. We found out just a couple of weeks ago that Charleigh and I were expecting a baby together and we've been really taken back by just how impressive the Australian Healthcare system can be. Within a day of having confirmation from our doctor that we were indeed expecting, Charleigh had already completed her first round of blood tests and we were booking ourselves back with our GP to hear the results.

The scan itself was incredibly impressive - I've personally had an ultrasound before, so the technology was not particularly new to me, but the amount of detail we could see was mind-blowing. More so than the detail of the images, was how much (or indeed how little) of the baby has formed. To our surprise the little monster measured in at 0.88cm in length, still attached to it's yolk sack it doesn't even have a discernible shape, but we could already make out it's heartbeat, measuring at 130<em>bpm. </em>We're told that in two weeks time, the baby will already have begun growing limb-buds and just a few days after that will be a fully formed foetus.

Whilst we do have a wonderful family already, this part of the process is entirely new to me and I'm really looking forward to what is going to happen over the coming weeks. We're scheduled back in at the doctors tomorrow for a walk through of the ultrasound scans Charleigh had, so I guess we'll learn a little more about the next steps then.

We already have a hospital "assigned" where the baby will be born (Assuming the pregnancy goes to the plan of both us and our doctor), so I can only imagine that we'll be taking it easy for the next few weeks before our next scan is booked in so that we can find out the gender!

Since we're so early in finding a lot of this out, I think we're both slightly confused as to what we're supposed to be doing next... It'll be months before we need to start looking at cots, moses baskets and clothing etc. but I already find myself trawling around furniture and baby sites, filled with anticipation for what will be happening around the beginning of May next year.

Before all of that however comes one of the most anticipated parts of pregnancy... Picking a name! We've been hypothetically talking about baby names for quite a while now, so we do have some sort of a shortlist in mind. We're continuing to work on that, but no doubt whatever name we ultimately pick (Whether that's before or after we know the gender), is likely to be completely abandoned the moment our baby is born.
