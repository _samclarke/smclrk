---
layout: post
title: Why I want nothing more than Facebook home
date: 2013-11-19 22:05:19.000000000 +11:00
---
<p style="text-align: center;"><a href="http://smclrk.com/wp-content/uploads/2013/04/6904058468_b0c354f40d_b.jpg"><img class="wp-image-186 aligncenter" style="border: 0px;" alt="HTC One X" src="http://smclrk.com/wp-content/uploads/2013/04/6904058468_b0c354f40d_b.jpg" width="1024" height="768" /></a></p>
I've used iPhones consistently since their initial release almost 6 years ago... In my opinion, nothing compares; their design is impeccable, integration with the rest of my devices is second to none and requires no work around whatsoever and most importantly like all other smartphones gives me direct access to all and any social networks that I'm a part of.

All of that aside (lets be honest, no one wants to read another "iPhones are the best" post...), I carry 2 phones day to day; my personal iPhone 5 and my work's HTC One X. I detest carrying 2 phones, it has to be said. The only person who can genuinely warrant carrying more than one phone in their day to day life is either someone reviewing said phone, or a drug dealer. In spite of this, I do have to use my HTC now and again and it does make my life easier when it comes to separating work and home lives as much as possible.

The problem I have with my HTC is twofold - Since it's a company phone, I can't root it to run directly from the Android OS, I have to stick with this horrendous skin provided courtesy of Optus. Why on earth can I not just remove this useless barrier between my hand and the genuine power than the phone has to offer?! The second issue I have is that I'm constantly talking to people via Twitter/iMessage/Facebook on my iPhone that it has become my hub for social interaction.

The reason I'm looking forward to Facebook Home quite so much, is that it somewhat masks the useless Optus Sense skin from the HTC, but also makes the integration with Facebook second to none. From what I've seen it'll bring my work phone much more to life in day to day use, if I'm immediately connected to friends and family.

Since moving down under I've found myself using Facebook more and more for pictures, locations and updates (as I imagine Facebook should genuinely be used) and to make that process simple will ease the first world problems I find myself struggling with day to day.

<a href="http://www.flickr.com/photos/johnkarakatsanis/6904058468/in/photostream/">Image via johnkarakatsanis/Flickr</a>
