---
layout: post
title: Playing the waiting game
date: 2013-11-19 22:05:19.000000000 +11:00
---
Over this past weekend, I've spent a lot of time researching and looking at apartments/houses in Melbourne. I viewed a couple of different apartments on Saturday and then spent a lot of time looking round the different suburbs, even making my way in to the centre of Melbourne to take a look around! After all that, I've pretty much decided on the area I think we'd like to live in so I spent most of Sunday applying to various different estate agents for properties I'd like to see and even making applications for a few of them!<!--more-->
<br/><br/>Now comes the painful part... Waiting. The place I liked the most has been on the market for around 4 weeks now, without anyone having made an offer - Which I see as a good thing! I made an offer on Saturday after viewing and am really keen to make it our home! The estate agent reckons that my application will be processed today and they'll run through reference checks etc. I'm pretty keen to have the application happen as quickly as possible, so as soon as I can, I shall be ringing the agent!
