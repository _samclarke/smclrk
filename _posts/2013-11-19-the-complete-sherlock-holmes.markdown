---
layout: post
title: The Complete Sherlock Holmes
date: 2013-11-19 22:05:19.000000000 +11:00
---
<p style="text-align: center;"><a href="http://smclrk.com/wp-content/uploads/2013/05/The-Complete-Sherlock-Holmes.jpg"><img class="wp-image-199 aligncenter" style="border: 0px;" alt="The-Complete-Sherlock-Holmes" src="http://smclrk.com/wp-content/uploads/2013/05/The-Complete-Sherlock-Holmes.jpg" width="1024" height="683" /></a></p>
Throughout my life I've constantly had trouble with reading.. Now, that's not to say I can't read, of course I can. My problem has always been trying to find something fictional that actually grips me and I find myself drawn toward constantly picking up.

I've always been massively in to reading factual books; Biographies, Documentaries etc. but for some reason, fiction and fantasy have never been able to stick. That said, one set of stories is an exception to this rule, any form of Sherlock Holmes story (Whether that be an original Conan Doyle or a new take on an original concept such as House). Charleigh recently bought me the entire library of Sherlock Holmes stories by Sir Arthur Conan Doyle, only problem is, I'm yet to pick it up!

This is my second problem - I feel that I suffer from a mild case of FOMO and as such rarely find myself without my laptop, iPad or iPhone within a short grasp. I must find the time for these wonderful stories, my complete and utter lack of imagination require it.
