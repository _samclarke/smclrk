---
layout: post
title: 'Trip to Hong Kong: The Gardens'
date: 2013-12-10 21:20:32.000000000 +11:00
---
This evening I got to spend a couple of hours wondering around the beautiful Hong Kong Park. It was wonderful to see a little paradise in the midst of a vast concrete jungle. I snapped a few pictures and they're over on Flickr for you to take a look at. (Click below).

{% image http://farm4.staticflickr.com/3668/11305379383_f75151ff3a_b.jpg %}