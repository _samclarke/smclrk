---
layout: post
title: Never go on holiday
date: 2013-11-19 22:05:19.000000000 +11:00
---
It's been said many times, if you go on holiday you won't have a job to come back to. That's really not so, but the case here is if you go on holiday, you'll wish you hadn't!<br/><br/>I have a fairly high workload, I always have... But that's not for lack of trying hard, it's just the way this industry works. I constantly have an (over capacity) 10 clients, alongside my work helping colleagues, plus general day to day house keeping. So when I miss a day, in this case two, I have to cram 5 days work in to 3, which just doesn't look healthy!<br/><br/>I'm constantly working evenings and weekends, always learning more and trying new things. But hell, do I hate having time off!
