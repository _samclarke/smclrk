---
layout: post
title: On Apple and the iPhone 5
date: 2013-11-19 22:05:19.000000000 +11:00
---
I've always been fairly hesitant to write anything about subjects like this, mainly because so many other people do and quite frankly, I just don't want their opinions...

That said, a friend and I were just discussing the announcement of the new iPhone and this was my response to him.
<blockquote>I've gotta be honest - I am disappointed, but I'm not as disappointed with the phone, so much the poor manor in which they handled the leaks.. If we didn't know anything 6 weeks ago (As with the launch of the first phone for instance), what we found out would have been a much better presentation... That said, you're right, the phone is still lacking - But the only way in which I think it's lesser than lets say the SGS3, is NFC...

Anything else we're looking at in comparison is pretty much OS based, right? And changes for that are easy to make.. So perhaps it's not missing out on so much?

I'm blabbering slightly, but also defending a brand and product I love... I will get an iPhone 5, it's inevitable... But like I say, I also have an HTC One X for work, so what am I missing out on really?

Again - I bought in to the Apple family a long time ago and as a result, my changing from an iPhone to anything else at this point, would render my synchronous world fairly useless... I'm going to buy a Galaxy Nexus tablet and I do have a PC - So I'm not entirely drowned in Apple products.. But, this product launch just wasn't the same, and I put that down to leadership.</blockquote>
For me, it's a family of products and comparing one manufacturer of a phone to another is an exercise in futility. If I wanted a Samsung Galaxy, I'd get one. I just don't want it. I want my iPhone, my iPad and Macbook Pro and I want the ease of use I've come to love. I want to not have to worry about syncing devices, keeping OS' up to date, nor do I want to have 4 different operating systems to live with.

Most people's decisions in life are aimed at making their lives easier, to allow them to focus on the things that really matter. To me, I couldn't care less that the SGS3 or the new Nokia have NFC - I'm really not bothered... My cards are NFC enabled and that's all that matters. If one day Apple do release a phone with that capability, then great. But for now, who really cares?
