---
layout: post
title: Where the hell have I been?
date: 2013-11-19 22:05:19.000000000 +11:00
---
Sat here, half way through finishing off a post I shall publish in the coming days, I realised I hadn't posted on here for quite some time! As such, there is a hell of a lot to write about.. That said, I will compress this astounding story in to a small number of paragraphs, just the right size for my attention span to cope with.<!--more-->


Since last writing, we have acquired our apartment and I am proud to say, it is the perfect place. The apartment we were hoping for, is all ours. Just to lay the ground rules, I won't be posting any pictures of the place.. Nor will I be describing it, in any detail at all, until Charleigh and Ellie are here with me. We're currently waiting on their VISAs to be approved but we're oh so very close now!

Moving away from setting ourselves up together, I've pretty much been working non-stop since I got here! I decided not to take off any time, so that I could save that for my families arrival. I have however, been fortunate enough to make my way up to Sydney for Online Retailer conference... I got myself up there the weekend prior to the conference, which gave me plenty of spare time. I'm sorry to say, I fell in to the category of "The Tourist". Not, I might add, with the flare of Johnny Depp in Italy, more so with the aspiration to not stop moving. I've found that to be a fantastic way to keep going while our family is not yet together and more recently I'd been struggling quite severely. So it was nice to spend a weekend and then the following couple of days of the conference on the tip of my toes.

I walked, I believe, on nearly every street in Central Sydney, of course, made my way to the Apple Store, Westfield and just about any other shop I could find! I managed to get some shopping out the way and even treated myself to a few things! I also walked my way through town to the Sydney Opera House &amp; Harbour Bridge which are spectacular. I was surprised by them... They're such well known structures that I think it's safe to say most people have seen them. On that basis, I assumed they'd be somewhat a let down... I'll let you make up your own mind if you're ever so fortunate.

I now find myself, back at my desk in Melbourne... Waiting.
