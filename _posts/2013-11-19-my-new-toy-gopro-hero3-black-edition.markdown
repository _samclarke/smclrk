---
layout: post
title: 'My New Toy: GoPro Hero3 Black Edition'
date: 2013-11-19 22:05:19.000000000 +11:00
---

For the past couple of months I've been lusting over a Go Pro. For years now I've had an affinity with life-logging, I think I've proven that  to myself with the array of cameras I've owned along with the amount I constantly update/check-in, but this time I wanted to move to the next level.

In a months time I'm meeting my brother in LA for our next annual road trip! Last year we headed to Europe and managed to drive through 5 countries in 4 days, this time we're renting a car on the west coast and aiming to see as much as possible of the US inside the week we've given ourselves! The last trip we made I bought an HD video camera for and we managed to get some pretty awesome shots, but unfortunately my capacity for video editing was pretty limited at the time,  that along with computer/hard drive changes, meant I lost the majority of the footage I got. This time however I plan for that to be different!

Along with the Go Pro, I also bought a suction cup mount and a head mount so the hope is to get as much footage (good or otherwise) that I can then use to start putting together decent videos.
