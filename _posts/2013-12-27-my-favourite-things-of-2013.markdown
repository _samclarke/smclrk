---
layout: post
title: My Favourite Things of 2013
date: 2013-12-27 20:46:50.000000000 +11:00
tags: http://samclarke.ghost.io/content/images/2014/Jan/20_Week_Scan.jpg
---


2013 has been a fantastic year for myself and my family, and I really can't wait for what's to come in the new year. In keeping with usual internet tradition, I thought I'd put together my top few moments of this year (in no paticular order).

![Nexus 5](http://samclarke.ghost.io/content/images/2014/Jan/Nexus_5.jpg)

####Moving to Android

I spoke at length in a previous post about my planned move away from iPhone, it's something I'd been toying with the idea of for a little while, and now I've lived for almost a month with my Nexus 5, I can safely say it's been the most positive experience I've had with a phone since the original iPhone back in 2007. I have to give a large amount of the credit for both the easy transition and the continued love for the platform, over to Google. Such a tight integration to apps I use daily is incredibly useful and pushes Android lightyears ahead of iOS.

![20 Week Ultrasound Scan](http://samclarke.ghost.io/content/images/2014/Jan/20_Week_Scan.jpg)

####The announcement of our baby

The single greatest moment of my year was a short but sweet phone call from Charleigh - "We did it". Easily the biggest moment to look forward to in 2014!

####Our engagement

Really though, how could this not be one of my top moments? I finally asked Charleigh to be my wife and with a litle nerve racking hesitation, she said yes! We began planning the wedding almost immediately and already have a ton of ideas as to how we want to celebrate, where we want to get married and I'm pretty positive that Charleigh already has her dress and colour scheme all picked out.

![Hong Kong Gardens](http://samclarke.ghost.io/content/images/2014/Jan/11305379383_f75151ff3a_b.jpg)

####Travelling for business and pleasure

I've been really fortunate so far in life to travel quite a lot and this year has been no different. I travelled all over Australia, to Hong Kong and the US (This was just for work) and for a little leisure time, I met my brother in LA for a 2 week road trip around California. I can't wait to travel even more next year, with the whole family in tow!

![California Highway](http://samclarke.ghost.io/content/images/2014/Jan/California_Highway.jpg)

####Using my camera more

I'm not a huge fan of New Year resolutions, but if I had picked one at the beginning of 2013, it would have been to use my camera more. A mistake I'd made in the past was to assume taking a large quantity of pictures was equal to taking pictures of a high quality. This year I tried to photograph as though I was using a film Camera - Not clicking away 10 times every time I saw something interesting, instead trying to capture a moment as best I could. It's something I really spent a lot of time working on and the photosets I've been able to put together towards the end of 2013 have been some of my best and favourite to date.
