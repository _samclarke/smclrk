---
layout: post
title: The perfect interview? Look no further
date: 2013-11-19 22:05:19.000000000 +11:00
---
I've always been a massive fan of watching/listening to interviews, I love to learn more about people, especially the people I deem to be idols (In one form or another). Through the years I've been watching interviews online, one host has stood head and shoulder above the rest when it comes to preparation, knowledge of his guests, depth of his questions and his ability to retort to answers he would have not heard before hand.


That host is Jian Ghomeshi of QTV on CBC Radio (Canada). Every time I've watched or listened to him, I've been utterly amazed with how he's able to hold an incredibly deep, yet viewer-grasping conversation with his guests. Even to the point that I'll happily sit and listen to him quiz a guest I perhaps have no interest in.

Recently I watched him command the show whilst interviewing who is undoubtedly one of the most revered hosts and interviewers, Larry King. I watched in utter amazement how easily he seemed to extract answers from someone who is notorious for hard-set interviews.

My favourite to date however (And this could be slightly biased on the premise I love him anyway) was his 2011 interview with none other than Stephen Fry. Fry is well known as an eloquent, highly intelligent person as it is, but the grace with which Jian was able to question him, made Fry almost seem mortal.

If you get chance, I highly recommend giving a listen to any of his interviews (Though, perhaps not with the Arctic Monkeys, despite Jian's best efforts, the band were clearly elsewhere during the interview). I'll leave you with my favourite, what I have found to be the perfect interview with Stephen Fry.

###Update:

I should probably add here, I do have one more hero when it comes to interview methods. That is, James Lipton of Inside the Actors Studio. That said, his interview technique is really rather specific to actors, he's not as much of an all rounder and Jian Ghomeshi.

<iframe width="640" height="360" src="//www.youtube.com/embed/Y4y3s0SxYWU?rel=0" frameborder="0" allowfullscreen></iframe>