---
layout: post
title: 'Trip to Hong Kong: My First Day'
date: 2013-12-09 22:11:39.000000000 +11:00
---
<center><iframe src="http://www.flickr.com/photos/sam_clarke/11286162653/player/" width="750" height="500" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe></center>

I consider today to be my first day here in Hong Kong, although I did get to walk around and see a fair bit yesterday, in total I spent about 70% of my day getting to the hotel and recovering from a red-eye flight. Today I had plenty of time outside of work to take a look around.

This morning I was up at 6am sharp and by 7am was walking the streets of Hong Kong's Central district. I had 2 hours before I had to be at the office and so once I'd grabbed myself some breakfast, I spent my time wandering as many streets as possible. 

I decided before my trip that I was going to treat my camera as though it used film; I don't want to snap 1,000 pictures to later on pick out the best 50, instead I planned to really assess a scene before I decided to take a shot. So far, that's done me really well - having only taken 7 pictures on my Canon, I'm extremely pleased with how they've turned out. The picture above was taken at a bar I saw tonight whilst perusing the local market stalls. "Club 71" is rated really highly, it had a lovely feel, and their back wall covered in art and instruments.

After a succesful trip around the market stalls (I managed to get a little something for Ellie), I headed back to the hotel and I'm currently debating my choices for dinner. Did you know McDonalds deliver here..?!
