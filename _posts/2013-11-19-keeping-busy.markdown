---
layout: post
title: Keeping Busy
date: 2013-11-19 22:05:19.000000000 +11:00
---
When we first began planning the move to Australia, Charleigh and I both knew that during the interim, whilst we apart, we'd both have to keep pretty busy. Mainly for the sake of sanity, but also to be as effective as possible in the process of getting our move underway.<!--more-->
<br/><br/>I'm glad to say that so far, that seems to be working out really well! So far today I've viewed 2 different apartments, of which I've loved both! I've been invited to an AFL game this afternoon and I've got plenty of work to get done over the rest of the weekend!<br/><br/>Charleigh seems to be doing fantastically as well and I'm so very proud. There's a house viewing back home tomorrow and she's trying to work as much as possible between now and July when she will also be making the big move!<br/><br/>I'm so excited, I'm just really trying to make sure everything is ready for her and Ellie when they arrive! Though, it's pretty certain to say I won't be buying a car in any hurry! The average price of a car over here is insane compared to the UK! I guess that'll just have to stay on my "To-Do" list for now!
