---
layout: post
title: Time Management
date: 2013-11-19 22:05:19.000000000 +11:00
---
Last week, Charleigh and I had a long conversation about the different methods of time management, ranging from personal time to work time. Essentially what we were working on was an ideal way in which we could impart good life skills and practices on to Ellie without it seeming like that was our aim. We eventually came to a conclusion which we've started putting in to practise this week and seems to be working fairly well so far!<br/><br/>The long time we spent talking about different ways to approach this, led to me think about how I manage my time, whether or not it's the most effective method I could put in to practise and how I'd got to that conclusion in the first place!<br/><br/>To basically explain how I go about managing my work time, I thought I'd draw a little chart... Otherwise it could take me a lifetime to explain! haha.<br/><br/> 
