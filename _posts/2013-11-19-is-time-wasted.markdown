---
layout: post
title: Is time wasted?
date: 2013-11-19 22:05:19.000000000 +11:00
---
I've always been really good at managing my time, as I described in my last post I have a huge client list, but manage to stay on top of it whilst also helping out other colleagues. Sometimes though, I feel that my time would be better spent on other tasks?<br/><br/>I'm currently on my way down to London, on the 6:15 train out of Birmingham. From there I will catch the 9:12 Eurostar to Paris and head out for a 1pm meeting. <br/><br/>This is fine by me, it's always worth seeing a client, but I'll have spent the best part of 6 hours travelling this morning, through which I've been unable to do any work... This means that by the time I get back home (about midnight) I'll have a day to catch up on. <br/><br/>I can't help that feel, given the industry I work in, face to face client meeting are a slight waste of everyone's time? Maybe we should make better use of Skype or GoTo Meeting?
